package main

import (
	"log"
	"net/http"

	"gitlab.com/FiftyFiveTech/cost_of_delivery_golang_assignment/component"

	"github.com/maxence-charriere/go-app/v9/pkg/app"
)

func main() {
	app.Route("/", &component.CostCalculatorGUI{})

	app.RunWhenOnBrowser()

	http.Handle("/", &app.Handler{
		Name:        "Cost Calculator",
		Description: "Parcel Cost Calculator",
		Styles: []string{
			"https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic",
			"https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.css",
			"https://cdnjs.cloudflare.com/ajax/libs/milligram/1.4.1/milligram.css",
		},
	})

	if err := http.ListenAndServe(":8082", nil); err != nil {
		log.Fatal(err)
	}
}
