module gitlab.com/FiftyFiveTech/cost_of_delivery_golang_assignment

go 1.19

require github.com/maxence-charriere/go-app/v9 v9.6.7

require (
	github.com/google/uuid v1.3.0 // indirect
	github.com/stretchr/testify v1.8.0 // indirect
)
